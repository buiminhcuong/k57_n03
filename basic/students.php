<?php 
	$students = array(
		array('id' => 'A1', 'name' => 'Nguyen Van An', 'address' => 'Hanoi'),
		array('id' => 'A2', 'name' => 'Nguyen Van Binh', 'address' => 'Hanoi'),
		array('id' => 'A3', 'name' => 'Nguyen Van Chung', 'address' => 'Hanoi'),
		array('id' => 'A4', 'name' => 'Nguyen Van Duong', 'address' => 'Hanoi'));

	//1. In ra bảng danh sách sinh viên với 3 cột id, name, address
	//   | ID  |    Ho ten     |   Dia chi       |
	//   | 1   |Nguye Van An   |   Hanoi         |
	//   | 2   |Nguye Van Binh |   Hanoi         |
	//   | 3   |Nguye Van Chung|   Hanoi         |
	//   | ... |...            |...              |
	//
?>
<table>
	<tr>
		<td>ID</td>
		<td>Ho ten</td>
		<td>Dia chi</td>
	</tr>
	<?php foreach ($students as $index => $student){ ?>
		<tr>
			<td><?= $student["id"] ?></td>
			<td><?= $student["name"] ?></td>
			<td><?= $student["address"] ?></td>
		</tr>
	<?php } ?>
</table>

<?php
	//------------------------------------------
	//2. Xây dựng hàm printTable nhận vào mảng 2 chiều và mảng danh sách cột (kèm tiêu đề)
	//
	//Ví dụ:
	// printTable($students, 
	// 		array("id" => "Id", "address" => "Dia Chi", "name" => "Ho Ten"));

	// $products = array(
	// 	array('id' => 'A1', 'name' => 'Khan mat', 'price' => '10000'),
	// 	array('id' => 'A2', 'name' => 'Ao phong', 'price' => '200000'),
	// 	array('id' => 'A3', 'name' => 'Sua tuoi', 'price' => '44000'));
	// printTable($products, 
	// 		array("id" => "Id", "name" => "Ten SP", "price" => "Gia"));

	require 'lib/functions.php';

	printTable($students, ["id" => "Id", "address" => "Dia Chi", "name" => "Ho Ten"], "student-table");
?>
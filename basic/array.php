<?php 
	$a[0] = 1;
	$a[5] = 2;

	$a["hello"] = "world";
	$a[] = 3;
	$a[4] = 4;
	$a[] = 5;

	$b = array(0 => 1, 5 => 2, 
		"hello" => "world", 3, 
		4 => 4, 7);

	unset($b["hello"]);
?>

<hr/>
<h1>Cach 1</h1>
<table>
	<tr>
		<td>Key</td>
		<td>Value</td>
	</tr>
<?php 
	foreach ($a as $k1 => $v1) {	
		echo("<tr>
			<td>$k1</td>
			<td>$v1</td>
		</tr>");
	}
 ?>
</table>

<hr/>
<h1>Cach 2</h1>
<table>
	<tr>
		<td>Key</td>
		<td>Value</td>
	</tr>
<?php foreach ($b as $k1 => $v1) { ?>
		<tr>
			<td><?=$k1?></td>
			<td><?=$v1?></td>
		</tr>
<?php } ?>
</table>

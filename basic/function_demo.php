<?php 
	function sum($a, $b = 20) {
		return $a + $b;
	}

	echo sum(5, 10). "</br>";
	echo sum(8) . "<br/>";

	$s = 10;
	$s2 = 30;

	function f1() {
		global $s;
		echo("$s <br/>");
	}

	function f2() {
		$l = 20;
		echo($GLOBALS["s2"]);
	}

	f1();
	f2();
	echo($l);
?>
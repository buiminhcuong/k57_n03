<?php 
	function printArray($a) {
		echo("<table>
			<tr>
				<td>Key</td>
				<td>Value</td>
			</tr>");
		
		foreach ($a as $k1 => $v1) {	
			echo("<tr>
				<td>$k1</td>
				<td>$v1</td>
			</tr>");
		}
		
		echo ("</table>");
	}

	function printTable($data, $columns, $className = "default") {
		echo("<table class=\"data-table $className\">
			<tr class=\"header $className\">");
		foreach ($columns as $title) {
			echo("<td>$title</td>");
		}
		echo "</tr>";
		foreach ($data as $row) {
			echo "<tr class=\"data-row $className\">";
			foreach ($columns as $field => $title) {
				echo("<td>$row[$field]</td>");
			}
			echo "</tr>";
		}
		echo "</table>";
	}
 ?>
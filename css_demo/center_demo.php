<style type="text/css">
	body {
		margin: 0px;
		vertical-align: middle;
	}

	#outter {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		width: 100px;
		height: 100px;
		margin: auto;
		background-color: green;
		padding: 30px;
	}
</style>

<div id="outter">
	<div id="inner">
		inner
	</div>
</div>
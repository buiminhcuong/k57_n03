<?php 
require_once '../lib/db.php';
require '../lib/cat_service.php';
include 'includes/header.php';
if (isset($_POST["add"])) {
	$conn = db_connect();
	$title = db_escape_postparam($conn, "title");
	$description = db_escape_postparam($conn, "description");

	addCat($conn, $title, $description);
	
	echo("Record is added successfully");

	db_close($conn);
}
?>

<div id="content">
	<div id="left">
		<?php include 'includes/menu.php';?>
	</div>
	<div id="main">
		<a href="cat.php">Back to list</a>
		<form method="POST">
			<table>
				<tr>
					<td>Title</td>
					<td><input type="text" name="title" required></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><textarea name="description"></textarea></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" name="add"></td>
				</tr>
			</table>
		</form>
	</div>
</div>
<?php include 'includes/footer.php'; ?>
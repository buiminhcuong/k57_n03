<?php 
require_once '../lib/db.php';
require '../lib/cat_service.php';
include 'includes/header.php';
$conn = db_connect();

$id = db_escape_getparam($conn, "id");

if(isset($_POST["edit"])) {
	$title = db_escape_postparam($conn, "title") ;
	$description = db_escape_postparam($conn, "description");

	$result = editCat($conn, $id, $title, $description);

	echo("Record is updated successfully");
}

$record = getCat($conn, $id);
db_close($conn);

 ?>

<div id="content">
	<div id="left">
		<?php include 'includes/menu.php';?>
	</div>
	<div id="main">
		<a href="cat.php">Back to list</a>
		<form method="POST">
			<table>
				<tr>
					<td>Title</td>
					<td><input type="text" name="title" value="<?=$record["title"]?>" required></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><textarea name="description"><?=$record["description"]?></textarea></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="submit" name="edit" value="Edit">
						<a href="cat_delete.php?id=<?=$record["id"]?>" onclick="confirm('Are you sure?')">Delete</a>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<?php include 'includes/footer.php'; ?>
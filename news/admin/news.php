<?php include 'includes/header.php'; ?>

<?php 
	require_once '../lib/db.php';
	require '../lib/news_service.php';
	require '../lib/cat_service.php';
	require("../lib/controls.php");
	$conn = db_connect();
	$selectedCat = isset($_GET["cat"]) ? $_GET["cat"] : "";
?>

<div id="content">
	<div id="left">
		<?php include 'includes/menu.php';?>
	</div>
	<div id="main">
		<a href="news_add.php">Add</a> <br/>
		<form method="GET">
			<?php printCombobox(getCatList($conn), "cat", "Lọc theo nhóm tin", false, $selectedCat);  ?>
			<input type="submit" name="filter" value="Filter">
		</form>

	<?php 
		$result = getNewsList($conn, $selectedCat);	
		
		printTable($result, 
			["title" => "Title", 
			"summary" => "Summary",
			"content" => "Content" ,
			"cat_title" => "Category" ,
			"creation_date" => "Creation Date"],
			"news_edit.php");

		db_close($conn);

	?>
	</div>
</div>

<?php include 'includes/footer.php'; ?>
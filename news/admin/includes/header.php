<!DOCTYPE html>
<html>
<head>
	<title>Administration</title>
	<link rel="stylesheet" type="text/css" href="content/style.css">
</head>
<body>
	<div id="header">
		<div id="login-info">
			<?php
				 include '../lib/auth.php';
			 ?>
			Hello <?= getLoggedInUser()?>, <a href="logout.php">logout</a>
		</div>
		Header
	</div>
<?php 
if(!checkLoggedIn()) {
	header("location:login.php");
}

 ?>
<a href="cat.php">Back to list</a>
<?php 
require_once '../lib/db.php';
require '../lib/cat_service.php';

if (isset($_GET["id"])) {
	$conn = db_connect();

	$id = db_escape_getparam($conn, "id");

	$result = deleteCat($conn, $id);
	header("location:cat.php");

	db_close($conn);
}
?>
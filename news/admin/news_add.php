<?php 
require_once '../lib/db.php';
require '../lib/cat_service.php';
require '../lib/news_service.php';
require("../lib/controls.php");
include 'includes/header.php';

$conn = db_connect();
if (isset($_POST["add"])) {
	$cat_id = db_escape_postparam($conn, "cat");
	$title = db_escape_postparam($conn, "title");
	$summary = db_escape_postparam($conn, "summary");
	$content = db_escape_postparam($conn, "content");

	addNews($conn, $cat_id, $title, $summary, $content);
	
	echo("Record is added successfully");
}
?>

<div id="content">
	<div id="left">
		<?php include 'includes/menu.php';?>
	</div>
	<div id="main">
		<a href="news.php">Back to list</a>
		<form method="POST">
			<table>
				<tr>
					<td>Category</td>
					<td>
						<?php printCombobox(getCatList($conn), "cat", "Chọn nhóm tin"); ?>
					</td>
				</tr>
				<tr>
					<td>Title</td>
					<td><input type="text" name="title" required></td>
				</tr>
				<tr>
					<td>Summary</td>
					<td><textarea name="summary" style="height: 100px; width: 400px;" required=""></textarea></td>
				</tr>
				<tr>
					<td>Content</td>
					<td><textarea name="content" style="height: 200px; width: 400px;" required=""></textarea></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" name="add"></td>
				</tr>
			</table>
		</form>
	</div>
</div>

<?php 
db_close($conn);
?>

<?php include 'includes/footer.php'; ?>
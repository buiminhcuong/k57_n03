<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="content/style.css">

</head>
<body>
	<?php 
	include '../lib/auth.php';
	include '../lib/controls.php';

	if(isset($_POST["login"])) {
		$username = $_POST["username"];
		$password = $_POST["password"];

		if(doLogin($username, $password)) {
			header("location:cat.php");
		}
		else {
			printError("Invalid username or password!");
		}
	}

	 ?>
	<div class="login-wrapper">
		<div class="login-header">
			Login
		</div>
		<div>
			<form method="POST">
				<table>
					<tr>
						<td>Username: </td>
						<td><input type="text" name="username" required=""></td>
					</tr>
					<tr>
						<td>Password: </td>
						<td><input type="password" name="password" required=""></td>
					<tr>
						<td></td>
						<td><input type="submit" name="login" value="Login"></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>
</html>
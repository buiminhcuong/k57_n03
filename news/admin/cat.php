<?php include 'includes/header.php'; ?>
<div id="content">
	<div id="left">
		<?php include 'includes/menu.php';?>
	</div>
	<div id="main">
		<a class="link-button" href="cat_add.php">Add</a>
		<?php 
			require_once '../lib/db.php';
			require '../lib/cat_service.php';
			require("../lib/controls.php");

			$conn = db_connect();

			$result = getCatList($conn);	
			
			printTable($result, 
				["title" => "Title", 
				"description" => "Description"],
				"cat_edit.php",
				"",
				$conf["theme"]);

			db_close($conn);
		?>
	</div>
</div>
<?php include 'includes/footer.php'; ?>
<?php 
function doLogin($username, $password) {
	if($username == "admin" && $password == "123") {
		startSession();
		$_SESSION["username"] = $username;
		return true;
	}
	return false;
}

function checkLoggedIn() {
	
	return isset($_SESSION["username"]) 
		&& $_SESSION["username"] != null;
}

function getLoggedInUser() {
	startSession();
	return $_SESSION["username"];
}

function logout() {
	startSession();
	session_destroy();
	header("location:login.php");
}

function startSession() {
	if(session_status() == PHP_SESSION_NONE) {
		session_start();
	}
}
?>
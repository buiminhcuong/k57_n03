<?php 
	function printTable($data, $columns, $editLink = "", $deleteLink= "", $className = "default") {
		echo("<table class=\"data-table $className\">
			<tr class=\"header $className\">");
		foreach ($columns as $title) {
			echo("<td>$title</td>");
		}
		if($editLink != "") {
			echo("<td></td>");
		}
		if($deleteLink != "") {
			echo("<td></td>");
		}
		echo "</tr>";

		while($row = mysqli_fetch_assoc($data)) {
			echo "<tr class=\"data-row $className\">";
			foreach ($columns as $field => $title) {
				echo("<td>$row[$field]</td>");
			}
			if($editLink != "") {
				echo("<td><a class=\"link-button\" href=\""  . $editLink . "?id={$row["id"]}" . "\">Edit</a></td>");
			}
			if($deleteLink != "") {
				echo("<td><a href=\""  . $deleteLink . "?id={$row["id"]}" . "\" onclick='return confirm(\"Are you sure?\")'>Delete</a></td>");
			}
			echo "</tr>";
		}
		
		echo "</table>";
	}


	function printCombobox($data, $name, $emptyText = "Select data", $valueRequired = true, $selectedValue = 0) {
		$requireText = $valueRequired ? "required=\"\"" : "";
		echo("<select name=\"$name\" $requireText>");
		echo("<option value=\"\">$emptyText</option>");
		while ($row = mysqli_fetch_assoc($data)) {
			$selectedText = "";
			if($row["id"] == $selectedValue) {
				$selectedText = "selected";
			}
			echo("<option value=\"{$row["id"]}\" $selectedText>{$row["title"]}</option>");
		}
		echo("</select>");
	}

	function printError($msg) {
		echo("<div class='msg-error'>$msg</div>");
	}
 ?>